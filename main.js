import * as exo1 from './exercices/exo-1.js'
exo1.boucle1();
exo1.boucle2();
exo1.boucle3();
exo1.boucle4();
exo1.boucle5();
exo1.boucle6();
exo1.boucle7();
exo1.boucle8();

import * as exo2 from './exercices/exo-2.js'
exo2.fonction1();
exo2.fonction2();
exo2.fonction3();
exo2.fonction4();
exo2.fonction5("babibou", 15);
exo2.fonction6("Térieur", "Alex", 36);
exo2.fonction7("femme", 18);
exo2.fonction8(4,6);