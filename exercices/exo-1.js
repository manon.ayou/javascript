    // Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
    //     l'afficher
    //     incrémenter de 1

export const boucle1 = () => {
for (let i=0; i<10;i++){
        console.log(i);
}   
console.log("-------------------");
}

// ma_fonction();

// export { ma_fonction};

    // Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant     que la première variable n'est pas supérieur à 20 :

    //     multiplier la première variable avec la deuxième
    //     afficher le résultat
    //     incrémenter la première variable

export const boucle2 = () => {
let x = 0;
let y = Math.floor(Math.random(0,100) * 100);
console.log(x);
console.log(y);

for (x=0;x<=20;x++){
    console.log(x * y);
}
console.log("-------------------");
}

    // Exercice 3 : Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100.  Tant que la première variable n'est pas inférieur ou égale à 10 :

    // multiplier la première variable avec la deuxième
    // afficher le résultat
    // décrémenter la première variable

export const boucle3 = () => {
let a = 100;
let b = Math.floor(Math.random(1,100)*100);
for (a=100;a>=10;a--){
    console.log("a=" + a);
    console.log(a * b);
}
console.log("-------------------");
}

    // Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :
        // l'afficher
        // l'incrementer de la moitié de sa valeur

export const boucle4 = () => {
    for (let i=1; i<10;i+=(i/2)){
            console.log(i);
    }   
console.log("-------------------");
}
    
    // Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

export const boucle5 = () =>{
    for (let i=1; i<=15; i++) {
        console.log("On y arrive presque... (" + i + ")" );
    }
    console.log("-------------------");
}

    //Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

export const boucle6 = () =>{
    for (let i=20; i>1; i--) {
        console.log("C'est presque bon... (" + i + ")" );
    }
    console.log("-------------------");
}

    //Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...
export const boucle7 = () =>{
    for (let i=1 ; i<100;i+=15) {
    console.log("On tient le bon bout... (" + i + ")" );
    }
    console.log("-------------------");
}

    //Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
export const boucle8 = () =>{
    for (let i=200 ; i>=0;i-=12) {
    console.log("Enfin !!! (" + i + ")" );
    }
    console.log("-------------------");
}