// Exercice 1 Faire une fonction qui retourne true.

export const fonction1 = () =>{
    let a = 5;
    let b = 3;

    console.log(a===b);
    console.log("-------------------");
}


// Exercice 2 Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
export const fonction2 = () =>{
    let ma_chaine_de_caracteres = function () {
    console.log("Je suis une chaîne de caractères.");    
    }  

    ma_chaine_de_caracteres();
}

// Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.
export const fonction3 = () =>{
    let bonjour = function (nom) {
    console.log("Bonjour " + nom + " !");
    }
    bonjour("Raphaël")
}

// Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux

export const fonction4 = () =>{
    let c = Math.floor(Math.random(1,100)*100);
    let d = Math.floor(Math.random(1,100)*100);
    
    if(c<d){
        console.log(c + " est plus petit que " + d + ".")
    } else if(c>d) {
        console.log(c + " est plus grand que " + d + ".")
    } else {
        console.log(c + " est égal à " + d + ".")
    }
}


// Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.

export const fonction5 = (a, b) =>{
    console.log (a + b);
}

// Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".
export const fonction6 = (a, b, c) =>{
    console.log ("Bonjour " + a + " " + b + ", tu as " + c + " ans.");
}


// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur

export const fonction7 = (genre, age_sujet) =>{
    if (genre == "homme" && age_sujet< 18){
        console.log("Vous êtes un " + genre + " et vous êtes mineur (moins de " + age_sujet + "ans).")
    }
    else if (genre == "homme" && age_sujet>= 18){
        console.log("Vous êtes un " + genre + " et vous êtes majeur (plus de " + --age_sujet+ "ans).")
    }
    if (genre == "femme" && age_sujet< 18){
        console.log("Vous êtes une " + genre + " et vous êtes mineur (moins de " + age_sujet + "ans).")
    }
    if (genre == "femme" && age_sujet>= 18){
        console.log("Vous êtes une " + genre + " et vous êtes majeure (plus de " + --age_sujet + "ans).")
    }
}


// Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.

export const fonction8 = (a=1,b=2,c=3) =>{
    console.log(a+b+c);
}